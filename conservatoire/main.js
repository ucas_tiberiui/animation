const world = document.getElementById('world');
const scene = document.getElementById('scene');
const peopleCircle = document.getElementById('people');
const stats = document.getElementById('stats');
const nonEu = document.getElementById('non-eu');
const ucasCon = document.getElementById('ucas-conservatoire');
const musicNotes = document.getElementById('music_notes');
const worldMap = document.getElementById('world-map');

const masterTl = gsap.timeline();
const intro = gsap.timeline();
const firstStat = gsap.timeline();

// fade in scene
intro.fromTo(scene, {
    opacity: 0,
}, {
    duration: 1,
    opacity: 1,
});


// Intro world
intro.fromTo([world, peopleCircle], {
    duration: 0.5,
    scale: 0,
    transformOrigin: "50% 50%",
},{
    duration: 0.5,
    scale:1,
    transformOrigin: "50% 50%",
    ease: "back.out(1.5)",
    stagger: 1
});

//first set of stat
intro.fromTo(nonEu, {
    opacity: 0,
    x: '-10%'
},{
    duration: 0.2,
    opacity:1,
    x: 0
});

intro.from(ucasCon, {
    opacity: 0,
    x: '-10%'
});

intro.from(musicNotes, {
    duration: 0.2,
    scale: 0,
    transformOrigin: "50% 50%",
    ease: 'back.out(1.5)'
});

intro.from(stats, {
    opacity: 0,
    x: "10%"
});

gsap.fromTo(worldMap, {
    transformOrigin: '0 0',
    x: '-793px'
},{
    duration: 10,
    transformOrigin: '0 793px',
    x: '0'
}).repeat(-1);